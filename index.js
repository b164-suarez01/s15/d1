console.log("Hello worlds");

let z = 1;
++z
z++;
console.log(z);
console.log(z++);
console.log(z);

console.log(z++);
console.log(z);

console.log(++z);


let numA = '10';
let numB = 12;

let coercion = Number(numA) + Number(numB);
console.log(coercion);

let numE = true + 1;
console.log(numE);

console.log(0===false);
numstring = "5500";
console.log(numstring < "Jose");


let isAdmin = false;
let isRegistered = true;
let isLegal = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);

let userName = "gamer2022";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;
let requiredAge = 18;


let registration1 = userName.length > 8 && userAge >=requiredAge;

console.log(registration1);

let numG = -1;
if (numG <0) {
	console.log('Hello');
}
function addNum (num1,num2){
	if (typeof num1 ==="number" && typeof num2 === "number"){
		console.log(num1 + num2);
	}
	else{
		console.log("One of the arguments is not a number");
	}
}

addNum(5,"2");

function login(username,password){
	if (typeof username ==="string" && typeof password ==="string"){
		
		if(password.length >= 8 && username.length >=8){
			console.log('Thank you for logging in.')
		}
		else if (username.length < 8 && password.length >= 8){
			console.log('Username is too short.');
		}
		else if (password.length < 8 && username.length >= 8){
			console.log('Password is too short.');
		}
		else {
			console.log('Credentials are too short.');
		}
}
	else{
		console.log("one of the arguments is not a string.");
	}
}

login("Jadh","1dsa");

let message = "No  message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed <30){
		return "not a typhoon yet."
	}
	else if(windSpeed <=61) {
		return "Tropical depression detected";
	}
	else if(windSpeed >=62 && windSpeed <=88) {
		return "Tropical storm detected";
	}
	else if(windSpeed >=89 && windSpeed <=117) {
		return "Severe Tropical storm detected";
	}
	else{
		return "Typhoon Detected"
	}
}
message = determineTyphoonIntensity(68);
console.log(message);

if (message =="Tropical storm detected"){
	console.warn(message)
}

if(false){
	console.log('Falsy');
}

let ternaryResult = (1<18) ? true: false;
console.log (`Result of ternary operator ${ternaryResult}`);

// let a = 7;

// 7 === 5 ?
// console.log("A") : 
// (a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"));

// let name;

// function isOfLegalAge() {
// 	name = "John";
// 	return "You are of the legal age limit.";
// }

// function isUnderAge(){
// 	name = "Jane";
// 	return "You are under the age limit";
// }

// let age = parseInt(prompt("What is your age?"));
// console.log(age);

// let legalAge = (age>18)? isOfLegalAge() : isUnderAge();
// console.log(`Result of ternary Operator in functions: ${legalAge}, ${name}`);


// MiniActivity 03092022
// let dayToday =prompt("What is the day today?");



let colorOfTheDay = function(dayToday) {
 	
			if(dayToday.toLowerCase()==='monday'){
				alert(`Today is ${dayToday}, Wear Black`);
			}
			else if(dayToday.toLowerCase()==='tuesday'){
				alert(`Today is ${dayToday}, Wear Green`);
			}		
			else if(dayToday.toLowerCase()==='wednesday'){
				alert(`Today is ${dayToday}, Wear Yellow`);
			}
			else if(dayToday.toLowerCase()==='thursday'){
				alert(`Today is ${dayToday}, Wear Red`);
			}	
			else if(dayToday.toLowerCase()==='friday'){
				alert(`Today is ${dayToday}, Wear Violet`);
			}
			else if(dayToday.toLowerCase()==='saturday'){
				alert(`Today is ${dayToday}, Wear Blue`);
			}
			else if(dayToday.toLowerCase()==='sunday'){
				alert(`Today is ${dayToday}, Wear White`);
			}
			else {
			alert(`Invalid input. Enter a valid day of the week.`);	
			}
};
// colorOfTheDay(dayToday);

let hero = "Hercules";

switch (hero){
	case "Jose Rizal":
		console.log("National Hero of the Philippines");
		break;
	case "George Washington":
		console.log("Hero of the American Revolution");	
		break;
	case "Hercules":
		console.log("Legendary Hero of the Greek");
		break;
};

function roleChecker(role){
	switch(role){
		case "admin":
			console.log("Welcome Admin, to the dashboard.");
			break;
		case "user":
			console.log("You are not authorized to view this page.");
			break;
		case "guest":
			console.log("Go to the registration page to register.");
			break;
		default:
			console.log("Invalid Role");
	}
}

roleChecker("admin");


function showIntensityAlert(windSpeed){

	try{
		alerat(determineTyphoonIntensity(windSpeed));
	}catch(error){
		//error/err are commonl;y used by devs for storing errors
		console.log(typeof error);
		//Catch errors within 'try' statement
		console.warn(error.message);
		//the error message is used to access the info relating to an error object
	}
}
showIntensityAlert(68);

const number = 40;
try{
	if(number>50){
		console.log("Success");
	}else{
		throw Error ("The number is low");
	}
}
catch(error){
	console.log("An error is caught");
	console.warn(error.message);
}

function getArea(width,height){
	if(isNaN(width) || isNaN(height)){
		throw "Pareameter is not a number!"
	}
}
try{
	getArea(3,"A");
}
catch(e){
	console.error(e);
}
finally {
	alert("Number only");
}